defmodule IotEngine.DeviceTest do
  use ExUnit.Case

  alias IotEngine.Device

  describe "when the device hasn't been monitored yet" do
    setup do
      Device.unmonitor("foo")
      :ok
    end

    test "monitor works" do
      :ok = Device.monitor("foo")
    end

  end

  describe "when the device is already being monitored" do
    setup do
      Device.unmonitor("foo")
    end

    test "returns :error" do
      :ok = Device.monitor("foo")
      :error = Device.monitor("foo")
    end
  end


end
