defmodule IotEngine.DeviceSupervisor do
  alias IotEngine.Device

  use Supervisor

  # Client API
  def start_device(name) do
    case Supervisor.start_child(__MODULE__, [name]) do
      {:error, {:already_started, _pid}} -> :error
      _ -> :ok
    end
  end

  def stop_device(name) do
    _ = Supervisor.terminate_child(__MODULE__, pid_from_name(name))

    :ok
  end

  # Supervisor callbacks
  def start_link(_options) do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Supervisor.init([Device], strategy: :simple_one_for_one)
  end


  defp pid_from_name(name) do
    name |> Device.via_tuple() |> GenServer.whereis()
  end

end
