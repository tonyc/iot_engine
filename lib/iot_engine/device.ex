defmodule IotEngine.Device do
  @up_time_ms 5000

  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient
  require Logger

  alias IotEngine.DeviceSupervisor

  ### High-level public API Client functions

  def monitor(name) do
    DeviceSupervisor.start_device(name)
  end

  def unmonitor(name) do
    DeviceSupervisor.stop_device(name)
  end

  def offline?(name) do
    !online?(name)
  end

  def online?(name) do
    case Registry.lookup(Registry.Device, name) do
      [] -> false
      _ -> via_tuple(name) |> GenServer.call(:up?)
    end
  end

  def up?(name) do
    online?(name)
  end

  def ping(name) do
    name |> via_tuple() |> GenServer.cast(:ping)
  end

  def via_tuple(serial_number) do
    {:via, Registry, {Registry.Device, serial_number}}
  end

  ### GenServer callbacks
  def start_link(serial_number) when is_binary(serial_number) do
    GenServer.start_link(__MODULE__, [serial_number], name: via_tuple(serial_number) )
  end

  def init(_args) do
    {:ok, %{up: false}}
  end

  def handle_call(:online?, _from, state) do
    {:reply, state.up, state}
  end

  def handle_call(:up?, _from, state) do
    {:reply, state.up, state}
  end

  def handle_cast(:ping, state) do
    log "PING: <TBD>"

    state = Map.put(state, :up, true)
    state = Map.put(state, :last_ping, current_timestamp())

    if state[:offline_timer] do
      Process.cancel_timer state[:offline_timer]
    end

    state = Map.put(state, :offline_timer, schedule_offline_timer())

    {:noreply, state}
  end

  def handle_info(:go_offline, state) do
    state = Map.put(state, :up, false)

    log "OFFLINE: <TBD>"

    {:noreply, state}
  end

  defp schedule_offline_timer() do
    Process.send_after(self(), :go_offline, @up_time_ms)
  end

  defp log(msg) do
    Logger.info "[#{Timex.now}] [#{__MODULE__}] #{msg}"
  end

  defp current_timestamp do
    :os.system_time(:milli_seconds)
  end
end
