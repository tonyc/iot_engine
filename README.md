# IotEngine

This project models an IoT "device" pinging home and using a process to track
online/offline status.

Start monitoring a device with a name, and it will automatically go "offline"
after five seconds after being pinged:

```
iex(1)> alias IotEngine.Device
iex(2)> Device.monitor "bob"

15:07:56.420 [info]  [2017-09-05 22:07:56.415390Z] [Elixir.IotEngine.Device] Init: args: [name: "bob"]
{:ok}

iex(3)> Device.ping "bob"

15:07:59.156 [info]  [2017-09-05 22:07:59.155976Z] [Elixir.IotEngine.Device] PING: bob
:ok

iex(4)>
15:08:04.157 [info]  [2017-09-05 22:08:04.157149Z] [Elixir.IotEngine.Device] OFFLINE: bob
```

Retrieve a device's online status:
```

iex(5)> Device.online? "bob"
false
iex(6)> Device.ping "bob"

15:10:30.591 [info]  [2017-09-05 22:10:30.591763Z] [Elixir.IotEngine.Device] PING: bob
:ok

iex(7)> Device.online? "bob"
true
```

## Installation

```sh
mix deps.get
```

## Run it
```sh
iex -S mix
```
